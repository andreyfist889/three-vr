import { Object3D, Mesh, BoxGeometry, MeshBasicMaterial, TextureLoader } from 'three';

export default class Controls {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'controls';

    const deskButton = new Mesh(new BoxGeometry(0.38, 0.01, 0.08, 3, 3, 3), new MeshBasicMaterial({ map: new TextureLoader().load('assets/images/desk-button.png') }));
    const keyboardButton = new Mesh(new BoxGeometry(0.38, 0.01, 0.08, 3, 3, 3), new MeshBasicMaterial({ map: new TextureLoader().load('assets/images/keyboard-button.png') }));
    const monitorButton = new Mesh(new BoxGeometry(0.38, 0.01, 0.08, 3, 3, 3), new MeshBasicMaterial({ map: new TextureLoader().load('assets/images/monitor-button2.png') }));
    const mouseButton = new Mesh(new BoxGeometry(0.38, 0.01, 0.08, 3, 3, 3), new MeshBasicMaterial({ map: new TextureLoader().load('assets/images/mouse-button.png') }));

    deskButton.position.set(0, 0, 0);
    keyboardButton.position.set(0.4, 0, 0);
    monitorButton.position.set(0, 0.1, 0);
    mouseButton.position.set(0.4, 0.1, 0);

    deskButton.rotation.x = 90 * (Math.PI / 180);
    keyboardButton.rotation.x = 90 * (Math.PI / 180);
    monitorButton.rotation.x = 90 * (Math.PI / 180);
    mouseButton.rotation.x = 90 * (Math.PI / 180);

    this.object.add(deskButton);
    this.object.add(keyboardButton);
    this.object.add(monitorButton);
    this.object.add(mouseButton);

    this.object.rotation.y = 90 * (Math.PI / 180);

    this.object.position.set(-1, 1.1, -0.2);
  }
}
